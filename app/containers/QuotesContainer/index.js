/**
 *
 * QuotesContainer
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import QuotesComponent from 'components/QuotesComponent/Loadable';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectQuotesContainer from './selectors';
import reducer from './reducer';
import saga from './saga';
import './styles.scss';
import { quotes } from './quotes';

export function QuotesContainer() {
  useInjectReducer({ key: 'quotesContainer', reducer });
  useInjectSaga({ key: 'quotesContainer', saga });

  return (
    <div className="quotes-page">
      <div className="row search-header">
        <div className="col-sm-12 topic">
          Search any Quote:{' '}
          <input
            className="input-styling"
            type="text"
            placeholder="Enter any topic"
          />
          <input type="button" value="Search" className="search-styling btn-primary" />
        </div>
        <div className="col-sm-12 search-keyword">Showing Search results:</div>
      </div>

      <div className="quotes-container">
        <Helmet>
          <title>QuotesContainer</title>
          <meta name="description" content="Description of QuotesContainer" />
        </Helmet>

        {quotes.map((quote, index) => (
          <div className="quote " key={index}>
            <QuotesComponent
              quote={quote.Quote}
              author={quote.Author}
              isImageQuote={quote.ImageQuote}
              key={index}
            />
          </div>
        ))}
      </div>
    </div>
  );
}

QuotesContainer.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  quotesContainer: makeSelectQuotesContainer(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(QuotesContainer);
