import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the quotesContainer state domain
 */

const selectQuotesContainerDomain = state =>
  state.quotesContainer || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by QuotesContainer
 */

const makeSelectQuotesContainer = () =>
  createSelector(
    selectQuotesContainerDomain,
    substate => substate,
  );

export default makeSelectQuotesContainer;
export { selectQuotesContainerDomain };
