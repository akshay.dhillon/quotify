/**
 *
 * WhatsappShareButton
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
import './styles.scss';

function WhatsappShareButton(props) {
  let quote = "whatsapp://send?text="+props.quote;
  return (
    <span className="whatsapp-share-button">
      <a href={quote} data-action="share/whatsapp/share"><i className="fa fa-whatsapp fa-2x" /></a>
    </span>
  );
}

WhatsappShareButton.propTypes = {};

export default WhatsappShareButton;
