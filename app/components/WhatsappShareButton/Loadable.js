/**
 *
 * Asynchronously loads the component for WhatsappShareButton
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
