/**
 *
 * LikeButton
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';
import 'font-awesome/css/font-awesome.min.css';

function LikeButton(props) {
  return (
    <span className="like-button">
      {!props.isLiked && (
        <i className="fa fa-heart fa-2x" style={{ color: '#999999' }} />
      )}
      {props.isLiked && (
        <i className="fa fa-heart fa-2x" style={{ color: 'red' }} />
      )}
    </span>
  );
}

LikeButton.propTypes = {
  isLiked: PropTypes.string,
};

export default LikeButton;
