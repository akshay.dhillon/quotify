/**
 *
 * MenuBar
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';
import classNames from 'classnames';
import { createStructuredSelector } from 'reselect';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { makeSelectLocation } from './selector';
import './styles.scss';

function MenuBar(props) {
  const activeMenu = props.location.pathname;

  return (
    <div className="menu-bar">
      <nav className="navbar navbar-inverse">
        <div className="container-fluid">
          <div className="navbar-header">
            <button
              type="button"
              className="navbar-toggle"
              data-toggle="collapse"
              data-target="#myNavbar"
            >
              <span className="icon-bar" />
              <span className="icon-bar" />
              <span className="icon-bar" />
            </button>
            <a className="navbar-brand logo" href="#">
              Quotify
            </a>
          </div>
          <div className="collapse navbar-collapse" id="myNavbar">
            <ul className="nav navbar-nav">
              <li>
                <Link
                  className={classNames({ active: activeMenu === '/' })}
                  to="/"
                >
                  Home
                </Link>
              </li>
            </ul>
            <ul className="nav navbar-nav ">
              <li>
                <Link
                  className={classNames({ active: activeMenu === '/quotes' })}
                  to="/quotes"
                >
                  Quotes
                </Link>
              </li>
            </ul>
            <ul className="nav navbar-nav ">
              <li>
                <Link
                  className={classNames({
                    active: activeMenu === '/contact-us',
                  })}
                  to="/contact-us"
                >
                  Contact
                </Link>
              </li>
            </ul>
            <ul className="nav navbar-nav ">
              <li>
                <Link
                  className={classNames({ active: activeMenu === '/discover' })}
                  to="/discover"
                >
                  Discover
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
  );
}
const mapStateToProps = createStructuredSelector({
  location: makeSelectLocation(),
});

const withConnect = connect(mapStateToProps);

MenuBar.propTypes = {
  location: PropTypes.object,
};

export default compose(withConnect)(MenuBar);
