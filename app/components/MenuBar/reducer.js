/*
 * AppReducer
 *
 * The reducer takes care of our data. Using actions, we can
 * update our application state. To add a new action,
 * add it to the switch statement in the reducer function
 *
 */

import { fromJS } from 'immutable';
import { CHANGE_ACTIVE_MENU } from './constants';

// The initial state of the App
export const initialState = fromJS({
  isLoading: false,
  error: '',
  activeMenuItem: 'HomePage',
});

/* eslint-disable default-case, no-param-reassign */
function appReducer(state = initialState, action) {
  switch (action.type) {
    case CHANGE_ACTIVE_MENU:
      return state;
    default:
      return state;
  }
}

export default appReducer;
