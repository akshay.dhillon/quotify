/**
 *
 * Toolbar
 *
 */

import React, { memo } from 'react';
import 'font-awesome/css/font-awesome.min.css';
import LikeButton from 'components/LikeButton/Loadable';
import WhatsappShareButton from 'components/WhatsappShareButton/Loadable';
import FacebookShareButton from 'components/FacebookShareButton/Loadable';
import './styles.scss';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

function Toolbar(props) {
  return (
    <div className="toolbar">
      <LikeButton isLiked={false} />
      <WhatsappShareButton quote={props.quote} />
      <FacebookShareButton />
    </div>
  );
}

Toolbar.propTypes = {};

export default memo(Toolbar);
