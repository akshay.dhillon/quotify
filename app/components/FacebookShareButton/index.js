/**
 *
 * FacebookShareButton
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
import './styles.scss';

function FacebookShareButton() {
  return (
    <span className="facebook-share-button">
      <i className="fa fa-facebook fa-2x" />
    </span>
  );
}

FacebookShareButton.propTypes = {};

export default FacebookShareButton;
