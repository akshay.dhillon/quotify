/**
 *
 * Asynchronously loads the component for FacebookShareButton
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
