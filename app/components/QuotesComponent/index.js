/**
 *
 * QuotesComponent
 *
 */

import React from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
import Toolbar from 'components/Toolbar/Loadable';
import './styles.scss';

function QuotesComponent(props) {
  console.log(props);
  let quoteImg = props.quote;
  console.log(quoteImg);
  return (
    <React.Fragment>

        {!props.isImageQuote && 
        (
        <div className="quotes-component">
        <i className="fa fa-quote-left" style={{ color: '#cdcdcd' }} /> 
        <div className="quote">{props.quote}</div>
        <br />
        <br />
        <div className="author">- {props.author}</div>

        <div className="toolbar-menu">
          <Toolbar quote={props.quote} />
        </div>
        </div>
        )
        }
        {props.isImageQuote && 
          <div className="quotes-component">
            <center><img src={require(""+quoteImg+"")} alt="hj" style={{maxWidth: '100%'}} /></center>
            <div className="toolbar-menu">
              <Toolbar quote={props.quote} />
            </div>
          </div>
        }

     
    </React.Fragment>
  );
}

QuotesComponent.propTypes = {};

export default QuotesComponent;
