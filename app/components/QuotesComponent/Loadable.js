/**
 *
 * Asynchronously loads the component for QuotesComponent
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
